// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alertTitle" : MessageLookupByLibrary.simpleMessage("¡Advertencia!"),
    "cancelButton" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "continueButton" : MessageLookupByLibrary.simpleMessage("Seguir"),
    "fakeWebsite" : MessageLookupByLibrary.simpleMessage("¡Bienvenido al sitio web falso! ¡Gracias por permitirme robar tu información!"),
    "polarText" : MessageLookupByLibrary.simpleMessage("El oso polar es un oso hipercarnívoro cuyo rango nativo se encuentra en gran parte dentro del Círculo Polar Ártico, que abarca el Océano Ártico, los mares circundantes y las masas de tierra circundantes. Es la especie de oso más grande que existe, así como el carnívoro depredador más grande que existe."),
    "returnHome" : MessageLookupByLibrary.simpleMessage("Vuelve a casa"),
    "title" : MessageLookupByLibrary.simpleMessage("Osos Polares"),
    "warningText" : MessageLookupByLibrary.simpleMessage("Este enlace no fue puesto aquí por el autor original, puede ser un virus. ¡Proceda con precaución!")
  };
}
