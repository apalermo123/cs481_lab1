// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alertTitle" : MessageLookupByLibrary.simpleMessage("Warning!"),
    "cancelButton" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "continueButton" : MessageLookupByLibrary.simpleMessage("Continue"),
    "fakeWebsite" : MessageLookupByLibrary.simpleMessage("Welcome to the fake website! Thanks for allowing me to steal your info!"),
    "polarText" : MessageLookupByLibrary.simpleMessage("The polar bear is a hypercarnivorous bear whose native range lies largely within the Arctic Circle, encompassing the Arctic Ocean, its surrounding seas and surrounding land masses. It is the largest extant bear species, as well as the largest extant predatory carnivore."),
    "returnHome" : MessageLookupByLibrary.simpleMessage("Return to home"),
    "title" : MessageLookupByLibrary.simpleMessage("Polar Bears"),
    "warningText" : MessageLookupByLibrary.simpleMessage("This link was not put here by the original author, may be a virus. Proceed with caution!")
  };
}
