import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'generated/l10n.dart';


void main() {
  runApp(MaterialApp(
    localizationsDelegates: [
      // 1
      S.delegate,
      // 2
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ],
    supportedLocales: S.delegate.supportedLocales,
    home: MyApp(),
  ));
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(S.of(context).title, style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 40),
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return AlertDialog(
                            title: new Text(S.of(context).alertTitle),
                            content: new Text(S.of(context).warningText),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              new FlatButton(
                                child: new Text(S.of(context).cancelButton),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              new FlatButton(
                                child: new Text(S.of(context).continueButton),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => new SecondRoute()),
                                  );
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Text(
                        'totallynotafakelinkaboutpolarbears.com',
                        style: TextStyle(
                          color: Colors.blue,
                        )
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );


    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(S.of(context).polarText,

        style: TextStyle(color: Colors.white),
        softWrap: true,
      ),
    );


    return MaterialApp(
        title: 'Group 4: Lab 1',
        home: Scaffold(
          backgroundColor: Colors.white10,
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text('Group 4: Lab 1'),
          ),
          body: ListView(
              children: [
                Image.asset(
                  'PolarBear.jpg',
                  width: 600,
                  height: 300,
                  fit: BoxFit.cover,
                ),
                titleSection,
                textSection,
              ]
          ),
        )
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Hackers Page',
        home: Scaffold(
          backgroundColor: Colors.white10,
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text('Hackers Page'),
          ),
          body: ListView(
              children: [
                Image.asset(
                  'hacker.png',
                  width: 600,
                  height: 400,
                  fit: BoxFit.cover,
                ),
                Text(S.of(context).fakeWebsite, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.white,),),
                FlatButton(onPressed: () {Navigator.pop(context);},
                  child: Text(S.of(context).returnHome, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.blue,),),
                ),
              ]
          ),
        )
    );
  }
}

